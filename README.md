# Measuring Occupational Similarity
This repository provides a snapshot of my work done as a research assistant at the INRS. The code can be found [here](https://gitlab.com/Vanguard-/inrs-dash). Sections 3 and 6 have complementary interactive plots.

## Summary
The objective of this project is to find a way to measure occupational similarity. Current standards are established by the O*NET database, sponsored by the US Department of Labor, and relies on the inputs of professionals and domain experts. The end goal is the construction of an occupational mobility model that can reflect the realities of the labor market. 

This project assumes that the American and Canadian labor markets can be properly modelled as diffusion processes, where nodes are occupations, and edges represent transitions between occupations. Moreover, we assume that connected components exist in the graph. The existence of a graph structure motivates our search for occupational similarity --- a first step towards modelling the transition dynamics. The current goal is to robustly identify clusters of occupations.

## Section 1: Problem Overview
![](assets/1-static-problem-overview.png)

## Section 2: Data Exploration
![](assets/2-static-data-exploration.png)

## Section 3: Data Transformation
![](assets/3-static-data-transformation.png)

[Interact with the PCA plots](https://gl.githack.com/Vanguard-/inrs-static-demo/raw/main/assets/3-data-transformation-pca-plots.html)

## Section 4: Distance Metrics Considered
![](assets/4-static-metrics.png)

## Section 5: Clustering
![](assets/5-static-clustering.png)

## Section 6: Neighborhood Approach
![](assets/6-static-neighborhood.png)

[Interact with the ridgeline plot](https://gl.githack.com/Vanguard-/inrs-static-demo/raw/main/assets/6-ridgeline-plot.html)

